
import cv2
import numpy as np
import time
import argparse
from flask import Flask, render_template, Response
from conexion import Deteccion
from flask_mail import Message
from flask_mail import Mail
from flask import request
import sqlite3
import logging
from smtplib import SMTPException
from threading import Thread
from flask import current_app
from flask_mail import Message
from flask import  copy_current_request_context
import threading
from twilio.rest import Client
import numpy as np 
import matplotlib.pyplot as plt
from pylab import *

app = Flask(__name__)

# own modules
import utills, plot
confid = 0.5
thresh = 0.5
mouse_pts = []

app.config.update(dict(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 587,
    MAIL_USE_TLS = True,
    MAIL_USE_SSL = False,
    MAIL_USERNAME = 'asolinal2020@gmail.com',
    MAIL_PASSWORD = 'adminsolinal2020',
))  
mail = Mail(app)
###client = Client()

#mail=Mail()

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email():
    msg = Message("Distanciamiento Social",
                        sender="asolinal2020@gmail.com",
                        recipients=['dfsaigua@espol.edu.ec'])
    msg.body = 'Estas personas están incumpliendo con el Distanciamiento....'
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()












def cargarMatrices():
    config_path = 'config/'
    mtx = np.load(config_path + 'mtx.npy')
    dist = np.load(config_path + 'dist.npy')
    newcameramtx = np.load(config_path + 'newcameramtx.npy')
    roi = np.load('config/roi.npy')
    guia = np.load('config/guia.npy')
    return (mtx,dist,newcameramtx,roi,guia)

  
def analizarImagen():
    contador=0
    model_path = './models/'
    output_dir = './output/'
    video = './data/pruebas.mp4'
    #rtsp = "rtsp://admin:Nedo2019@192.168.100.207:554/Streaming/Channels/301"

    # load Yolov3 weights
    weightsPath = model_path + "yolov3-tiny.weights"
    configPath = model_path + "yolov3-tiny.cfg"
    net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
    ln = net.getLayerNames()
    ln1 = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    (mtx,dist,newcameramtx,roi,guia) = cargarMatrices()
    vs = cv2.VideoCapture(video)

    # Get video height, width and fps
    height = (int(vs.get(cv2.CAP_PROP_FRAME_HEIGHT))) * 0.4
    width = (int(vs.get(cv2.CAP_PROP_FRAME_WIDTH))) * 0.4
    fps = int(vs.get(cv2.CAP_PROP_FPS))

    # Set scale for birds eye view
    # Bird's eye view will only show ROI
    scale_w, scale_h = utills.get_scale(width, height)

    tiempoR = 0
    tiempo_extra=0
    banderaCorreo = False

    while True:
        (ret, frame) = vs.read()

        if(ret!=False):
            frame = cv2.resize(frame, None, fx=0.4, fy=0.4)
            frame = cv2.undistort(frame, mtx, dist, None, newcameramtx)
            H, W,C = frame.shape

            src = np.float32(roi)
            dst = np.float32([[0, H], [W, H], [W, 0], [0, 0]])
            perspective_transform = cv2.getPerspectiveTransform(src, dst)

            pts = np.float32([guia])
            warped_pt = cv2.perspectiveTransform(pts, perspective_transform)[0]

            distance_w = np.sqrt((warped_pt[0][0] - warped_pt[1][0]) ** 2 + (warped_pt[0][1] - warped_pt[1][1]) ** 2)
            distance_h = np.sqrt((warped_pt[0][0] - warped_pt[2][0]) ** 2 + (warped_pt[0][1] - warped_pt[2][1]) ** 2)

            pnts = np.array(roi, np.int32)
            cv2.polylines(frame, [pnts], True, (70, 70, 70), thickness=2)


            #yolo
            blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416), swapRB=True, crop=False)
            net.setInput(blob)
            start = time.time()
            layerOutputs = net.forward(ln1)
            end = time.time()
            print(end - start)
            boxes = []
            confidences = []
            classIDs = []

            for output in layerOutputs:
                for detection in output:
                    scores = detection[5:]
                    classID = np.argmax(scores)
                    confidence = scores[classID]
                    # detecting humans in frame
                    if classID == 0:
                        if confidence > confid:
                            box = detection[0:4] * np.array([W, H, W, H])
                            (centerX, centerY, width, height) = box.astype("int")
                            x = int(centerX - (width / 2))
                            y = int(centerY - (height / 2))
                            boxes.append([x, y, int(width), int(height)])
                            confidences.append(float(confidence))
                            classIDs.append(classID)

            idxs = cv2.dnn.NMSBoxes(boxes, confidences, confid, thresh)
            boxes1 = []

            for i in range(len(boxes)):
                if i in idxs:
                    boxes1.append(boxes[i])
                    x, y, w, h = boxes[i]
            cantidad_detectadas=len(boxes1) #cantidad de personas detectadas
            # Here we will be using bottom center point of bounding box for all boxes and will transform all those
            # bottom center points to bird eye view
            person_points = utills.get_transformed_points(boxes1, perspective_transform)

            # Here we will calculate distance between transformed points(humans)
            distances_mat, bxs_mat = utills.get_distances(boxes1, person_points, distance_w, distance_h)
            risk_count = utills.get_count(distances_mat)

            frame1 = np.copy(frame)

            # Draw bird eye view and frame with bouding boxes around humans according to risk factor
            bird_image = plot.bird_eye_view(frame, distances_mat, person_points, scale_w, scale_h, risk_count)
            img = plot.social_distancing_view(frame1, bxs_mat, boxes1, risk_count)

            cv2.imshow('Bird Eye View', bird_image)
            cv2.imshow('Imagen', img)
            cv2.imwrite(output_dir + "frame%d.jpg" % contador, img)
            cv2.imwrite(output_dir + "bird_eye_view/frame%d.jpg" % contador, bird_image)
            imagenWeb = cv2.imencode('.jpg', img)[1].tobytes()
            contador = contador +1
            yield (b'--imagenWeb\r\n'b'Content-Type: image/jpeg\r\n\r\n' + imagenWeb + b'\r\n')
            
            if(risk_count[0]!=0):  #cantidad de personas que incumplen
                    
                banderaCorreo = True
            else:
                banderaCorreo = False

            if(banderaCorreo==True):
                if(tiempoR>0):
                    tiempoactual = time.time()
                    dif = tiempoactual - tiempoR
                    #Aqui se envia el correo
                    if(dif>1):
                        print('debe enviarse correo')
                        print(contador)
                        mail.init_app(app)
                        account_sid = 'AC60ba31df9b104d6e4ab50ec90f1e0aa6'
                        # Your Auth Token from twilio.com/console
                        auth_token  = '0a5199288c47806513002ff8243106ca'
                        client = Client(account_sid, auth_token)

                        message = client.messages.create(
                            to="+593961371622", 
                            from_="+19704091102",
                            body="Distanciamiento Social")

                        print(message.sid)

                        send_email()

                        #task_2 = ('','2020-08-04','09:90:20',1,6)
                        #create_task(task_2)
                        conn = sqlite3.connect("cafeteria.db")
                        cur = conn.cursor()
                        hora= time.strftime("%H:%M:%S")
                        fecha= time.strftime("%y-%m-%d")
                        
                        
                        cur.execute("INSERT INTO Deteccion VALUES(null,'20" + fecha + "','" + hora + "',2,2)")
                        #"""INSERT INTO Deteccion(id_deteccion,fecha,hora,personas_detectadas,personas_incumplen)                            
                         #   VALUES (%s,%s,%s,%s,%s)""", 
                          #  (30,fecha,hora,45,32))
                        #cur.execute("INSERT INTO " + Deteccion + " VALUES ('" + fecha + "')",
                        conn.commit()
                        #sender= threading.Thread(name='mail_sender',target=send_email())
                        #sender.start()
                        #msg = Message("Hola administrador",
                        #sender="asolinal2020@gmail.com",
                        #recipients=["dfsaigua@gmail.com"])
                        #msg.body = 'Bienvenid@ a j2logo'
                        #mail.send(msg)
                        #send_email('hola','asolinal2020@gmail.com','dfsaigua@gmail.com','bienvenido jfnjosdnrfvor',
                        #cc=None, bcc=None, html_body=None)
                        tiempoR =0
                else:
                    tiempoR = time.time()
            else: 
                tiempoR=0

    vs.release()
    cv2.destroyAllWindows()

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(analizarImagen(),
                    mimetype='multipart/x-mixed-replace; boundary=imagenWeb')


@app.route('/reporte')
def reporte():
    reportes=Deteccion.query.all()
    return render_template('reporte.html',reportes=reportes)


#def guardarobjeto_enviaremail(valor1,valor2):
 #   fecha=time.strftime("%d/%m/%y")
  #  hora=time.strftime("%H:%M:%S")
   # objeto=Deteccion()
    #return print(objeto.hora)

##@app.route('/reporte',methods = ["POST","GET"])
####def reporte_fechas(startfecha,endfecha):
    ##fechas=


#@app.route('/crear',methods = ['POST', 'GET'])
#def create():
 #   create_form=forms.CreateForm(request.form)
 #   if request.method == 'POST':
  #      user=Deteccion(create_form.fecha.data,
   #                     create_form.hora.data,
    #                    create_form.personas_detectadas.data,
     #                   create_form.personas_incumplen.data)
      #  db.session.add(user)
       # db.session.commit()
       # return render_template("reporte.html", form=create_form)


@app.route('/reporte', methods=['GET','POST'])
def consultarfecha():
    Date1 = str(request.form.get("Fechadesde",False)).format("yyyy/MM/dd")
    Date2 = str(request.form.get("Fechahasta",False)).format("yyyy/MM/dd")
    print(Date1)
    print(Date2)
    datos=Deteccion.query.filter(Deteccion.fecha>Date1).filter(Deteccion.fecha<Date2).all()
    print(datos)
    

    return render_template('reporte.html', datos=datos)


@app.route('/grafico', methods=['GET','POST'])
def consultargrafico():
    Date3 = str(request.form.get("fechagrafico",False)).format("yyyy/MM/dd")
    print(Date3)
    grafico=Deteccion.query.filter(Deteccion.fecha==Date3).all()
    #print(grafico)
    count=0

    return render_template('grafico.html', grafico=grafico)

    

    

def create_task(deteccion):
    """
    Create a new task
    :param conn:
    :param task:
    :return:
    """
    conn = sqlite3.connect("cafeteria.db")
    sql = ''' INSERT INTO Deteccion(id_deteccion,fecha,hora,personas_detectadas,personas_incumplen)
              VALUES(,?,?,?,?,?) '''
    cur = conn.cursor()
    
    #cur.execute("INSERT INTO Deteccion VALUES(10,'2020-12-04','02:34:34',1,6)")
    conn.commit()

    return cur.lastrowid














#def create():
    
    
    #objeto=Deteccion()
#if __name__== "__main__":
 #   analizarImagen()

#from datetime import date
 #   start=date(year=2016, month=11, day=1)
  #  end= date(year=2016,month=11,day=30)

   # posts=Post.query.filter(Post.post_time<=end).filter(Post.post_time>=start)


if __name__ == '__main__':
    mail.init_app(app)
    #app.run()



