from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
#from .main2 import app
import smtplib

app= Flask(__name__)
#SECRET_KEY='my_secret-key'
#MAIL_SERVER='smtp.gmail.com'
#MAIL_PORT=587
#MAIL_USE_SSL=False
#MAIL_USE_TLS=True
#MAIL_USERNAME='asolinal2020@gmail.com'
#MAIL_PASSWORD = 'adminsolinal2020'




app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///cafeteria.db'
db= SQLAlchemy(app)
migrate=Migrate(app,db)

class Deteccion(db.Model):
	id_deteccion = db.Column(db.Integer,primary_key=True)
	fecha = db.Column(db.String(20),nullable=True)
	hora = db.Column(db.String(20),nullable=True)
	personas_detectadas= db.Column(db.Integer,nullable=True)
	personas_incumplen= db.Column(db.Integer,nullable=True)


class Correo(db.Model):
	id_correo= db.Column(db.Integer,primary_key=True)
	correo = db.Column(db.String(120), nullable=False)
